random bad script(s) i made
===========================

i say they're bad but they work

### backup.sh

use it by doing `./backup.sh ~/dir1 ~/dir2 ~/file1`.
don't forget `chmod +x ...`.
but if you don't use bash, just to be safe do: `bash backup.sh ...`.

if you don't want a headache, i recommend not looking past line 20.

it accepts a list of files and/or directories (full path please) and saves them
to a git repo of your choosing -- creating a new directory each time, not overwriting.

choose your git repo by editing the variable `backup_repo` in the script.

it saves only to the master/main branch of your repo, but with about 5 seconds of
work, you make it change branches (i'll do that at some point).
if your repo's master isn't called main (as is the new default), change it in
the script by setting `master` to whatever it is.

the script also looks for some config files if you want to save those too (you
can choose not to save them by setting `save_config` to false in the script).
i'm only looking for 
- .vimrc
- .tmux.conf
- .gitconfig
- .bash\_profile
- .mavenrc
- .config/.
you can add your own somewhere around line 60.

there is also no error checking at all (i'll add that at some point).

