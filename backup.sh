# this is prolly really bad

# put backup repo's url here
backup_repo=git@gitlab.com:abhi.sinha/backup.git

# put the name of your 'master' here, some default to main now
master=main

# do you want to look for config files?
# these include:
#   .vimrc
#   .tmux.conf
#   .bash_profile
#   .gitconfig
#   .mavenrc
#   .config/
# you can add your own below (see line 58)
save_config=true




printf "you're on bash\n$(bash --version)\n\n"
printf "this was written on bash 5.2 :)\n\n"

if [ $# -eq 0 ]; then
  if [ $save_config = true ]; then
    printf "you're backing up only config files?\n\n"
  else
    printf "you're backing up nothing?\n"
    exit
  fi
fi


_date=$(date +'%Y-%m-%d-%H-%M-%S')
printf "backing up files on: $_date...\n\n"

tmpdir=/tmp/$_date
printf "creating tmp dir $tmpdir\n\n"
mkdir $tmpdir

cd $tmpdir
git clone $backup_repo && cd *
backup_dir=$(pwd)
printf "\n"
printf "backing up to $(git remote -v)\n\n"

mkdir $_date && cd $_date
backup_today=$(pwd)
printf "your stuff will be saved in $(pwd)\n\n"

printf "copying files: no guarantee that this is fast\n"
for i in $*; do
  f=${i%/}
  printf "copying files from: $f\n"
  cp -r $f .
  printf "\n"
done

if [ $save_config = true ]; then
  printf "copying config files\n"
  cp ~/.vimrc .
  cp ~/.tmux.conf .
  cp ~/.bash_profile .
  cp ~/.gitconfig .
  cp ~/.mavenrc .
  cp -r ~/.config .
fi

printf "saving to git\n"

cd $backup_dir && git add .
git commit -m "$_date"
git push origin $master
printf "\n"

printf "these are all your backups:\n$(ls -lG $backup_dir | tail -n +2)\n\n"
printf "these were backed up today:\n$(ls -alG $backup_today | tail -n +4)\n\n"

printf "cleaning up...\n"
rm -rf $tmpdir

printf "\n"
printf "ty for your business\n"

